<?php declare(strict_types=1);

//===================================================================================
// SOLUTIONS INTERFACE
//===================================================================================

// benchmarks performed on 6 core CPU @ 3.9GHz

//1.3s -> 1000000 elements
function maxProfit(array $pricesAndPurchases) : int
{
    $trader = new CrudeOilTrader($pricesAndPurchases);
    $trader->runSimulation();
    return $trader->getProfit();
    return 0;
}

// !!!assumption: it is assumed that a character insert position can be both in front
// and back of src string as it was not clearly stated in the requirements

//0.03s -> 300 char; 1.8s -> 3000 char
function stringCost(string $src,
                    string $tgt,
                    int $insertionCost,
                    int $deletionCost,
                    int $replacementCost) : int
{
    $lcs = longestCommonSubsequence($src, $tgt);
	$deletionNum = strLen($src) - $lcs;
	$insertionNum = strLen($tgt) - $lcs;
    
	// use replacement
	if ($replacementCost < $insertionCost + $deletionCost)
	{
		$remainder = abs($insertionNum - $deletionNum);
		$mandatoryCost = $remainder;
		($deletionNum <= $insertionNum)       ? 
            $mandatoryCost *= $insertionCost  :
            $mandatoryCost *= $deletionCost;
		return (max($deletionNum, $insertionNum) - $remainder) * $replacementCost + $mandatoryCost;
	}
    // don't use replacement
	else
	{
		return $deletionNum * $deletionCost + $insertionNum * $insertionCost;
	}
}

// 0.09s -> 100000; 0.94 -> 1000000
function incrementalMedian(array $values): array
{
    $out = array();
    $rm = new RunningMedian();
    
    foreach($values as $input)
    {
         array_push($out, $rm->step($input, false));
    }
    return $out;
}

//===================================================================================
// PROBLEM 1 ADDITIONAL METHODS & CLASSES
//===================================================================================

class CrudeOilAccount
{
    private $balance;
    private $amount;
    
    function __construct()
    {
        $this->balance = 0;
        $this->amount = 0;
    }
    
    function __destruct() {}
    
    public function getBalance() : int { return $this->balance; }
    
    public function buyCrudeOil(int $price, int $amount)
    {
        $this->balance -= $price * $amount;
        $this->amount += $amount;
    }
    
    public function sellCrudeOil(int $price)
    {
        $this->balance += $price * $this->amount;
        $this->amount = 0;
    }
    
    public function resetAccount()
    {
        $this->balance = 0;
        $this->amount = 0;
    }
}

class CrudeOilTrader
{
    private $marketPrediction;
    private $intervalList;
    private $actionList;
    private $tradingAccount;
    
    function __construct(array &$predictionList) 
    {
        $this->marketPrediction = $predictionList;
        $this->intervalList = array();
        $this->actionList = array();
        $this->tradingAccount = new CrudeOilAccount();
        
        $this->findIncDecIntervals();
        $this->mergeIntervals();
        $this->generateActionList();
    }
    
    function __destruct() {}
    
    public function runSimulation()
    {
        $this->tradingAccount->resetAccount();
        $dayID = 0;
        foreach($this->marketPrediction as $dayData)
        {
            $currentPrice = $dayData["price"];
            $this->tradingAccount->buyCrudeOil($currentPrice, $dayData["purchased"]);
            if ($this->actionList[$dayID] == "sell")
            {
                $this->tradingAccount->sellCrudeOil($currentPrice);
            }
            $dayID++;
        }
    }
    
    public function getProfit() : int { return $this->tradingAccount->getBalance(); }
    
    private function buildDefaultInterval () : array
    {
        return array('startValue' =>            0,
                     'startIndex' =>            0,
                     'endValue' =>              0,
                     'endIndex' =>              0,
                     'useSubRange' =>           0,
                     'subRangeStartIndex' =>    0,
                     'subRangeEndIndex' =>      0,
                     'state' =>                 0);
    }
    
    private function getCurrentState(int $currentPrice, int $nextPrice) : int
    {
        if ($currentPrice >= $nextPrice)    { return 0; }
        else                                { return 1; }
    }
    
    private function findIncDecIntervals()
    {
        $n = sizeof($this->marketPrediction);
        $maxValue = 0;
        $isDirectionChange = true;
        $tempInterval = $this->buildDefaultInterval();
        $state;

        for ($i = 0; $i < $n; $i++)
        {
            $currentPrice = $this->marketPrediction[$i]["price"];

            if ($i < $n - 1)
            {
                $nextPrice = $this->marketPrediction[$i + 1]["price"];

                if ($isDirectionChange)
                {
                    $state = $this->getCurrentState($currentPrice, $nextPrice);
                    $isDirectionChange = false;

                    $tempInterval['startIndex'] = $i;
                    $tempInterval['startValue'] = $currentPrice;
                    $tempInterval['state'] = $state;
                }

                $newState = $this->getCurrentState($currentPrice, $nextPrice);

                if ($state != $newState) 
                {
                    $isDirectionChange = true;
                    $tempInterval['endIndex'] = $i;
                    $tempInterval['endValue'] = $currentPrice;
                    array_push($this->intervalList, $tempInterval);
                }
            }
            else
            {
                if ($isDirectionChange)
                {
                    $state = $this->getCurrentState($this->marketPrediction[$i - 1]["price"], $currentPrice);
                    $isDirectionChange = false;
                    $tempInterval['startIndex'] = $i;
                    $tempInterval['startValue'] = $nextPrice;
                    $tempInterval['state'] = $state;
                }

                $tempInterval['endIndex'] = $i;
                $tempInterval['endValue'] = $currentPrice;
                array_push($this->intervalList, $tempInterval);
            }
        }
    }

    private function canMerge(int $currentID, array &$tempInterval) : bool
    {
        $mergeState = false;

        if ($tempInterval['state'] == 1)
        {
            // if inc inc
            if ($this->intervalList[$currentID]['state'] == 1)
            {
                if (($tempInterval['endValue'] < $this->intervalList[$currentID]['endValue']))
                {
                    $mergeState = true;
                }
            }
            // if inc dec
            else
            {
                //check if there is element at currentID + 1
                if ($currentID + 1 < sizeof($this->intervalList))
                {
                    if (($tempInterval['endValue'] > $this->intervalList[$currentID + 1]['startValue']) && ($tempInterval['endValue'] > $this->intervalList[$currentID + 1]['endValue']))
                    {
                        if ($this->intervalList[$currentID]['startValue'] != $this->intervalList[$currentID]['endValue'])
                        {
                            $mergeState = true;
                            $tempInterval['state'] = 2;
                        }
                        else { $mergeState = false; }
                    }
                    else { $mergeState = false; }
                }
                else { $mergeState = false; }
            }
        }
        else
        {
            // if dec inc
            if ($this->intervalList[$currentID]['state'] == 1)
            {
                if (($tempInterval['startValue'] <= $this->intervalList[$currentID]['endValue']))
                {
                    $mergeState = true;
                    $tempInterval['state'] == 1;
                    // check if interval start point is less than its end point
//
                    if ($tempInterval['startValue'] >= $this->intervalList[$currentID]['endValue'])
                    {
                        $tempInterval['useSubRange'] = true;
                        $tempInterval['subRangeStartIndex'] = $tempInterval['startIndex'];
                        while ($this->marketPrediction[$tempInterval['subRangeStartIndex']]["price"] >= $this->intervalList[$currentID]['endValue'])
                        {
                            $tempInterval['subRangeStartIndex'] = $tempInterval['subRangeStartIndex'] + 1;
                        }
                    }
                }
                else
                {
                    //print_r($tempInterval);
                    $state = false;
                    for ($i = $tempInterval['endIndex']; $i < $tempInterval['startValue']; $i--)
                    {
                        if ($this->marketPrediction[$i]["price"] < $this->intervalList[$currentID]['endValue']) 
                        {
                            $state = true;
                            break;
                        }
                    }

                    if ($state)
                    {
                        $tempInterval['useSubRange'] = true;
                        $tempInterval['subRangeEndIndex'] = $tempInterval['endIndex'];
                        // in [4, 2, 1] and [3] to get [4, 2]
                        while (($this->marketPrediction[$tempInterval['subRangeEndIndex']]["price"] < $this->intervalList[$currentID]['endValue']) && ($this->marketPrediction[$tempInterval['subRangeEndIndex'] - 1]["price"] < $this->intervalList[$currentID]['endValue']))
                        {

                            $tempInterval['subRangeEndIndex'] = $tempInterval['subRangeEndIndex'] - 1;
                        }
                    }
                    $mergeState = false;
                }
            }
            // if dec dec
            else
            {
                if (($tempInterval['endValue'] > $this->intervalList[$currentID]['startValue']) && ($tempInterval['endValue'] > $this->intervalList[$currentID]['endValue']))
                {
                    $mergeState = true;
                }
            }
        }
        return $mergeState;
    }

    private function mergeIntervals()
    {
        $tempInterval = $this->buildDefaultInterval();
        $filteredIncDecIntervals = array();

        for ($i = 0; $i < sizeof($this->intervalList); $i++)
        {
            if ($i == 0)
            {
                // new interval
                $tempInterval = $this->intervalList[$i];
            }
            else
            {
                if ($this->canMerge($i, $tempInterval))
                {
                    $tempInterval['endIndex'] = $this->intervalList[$i]['endIndex'];
                    $tempInterval['endValue'] = $this->intervalList[$i]['endValue'];
                    
                    // handle last interval
                    if ($i == sizeof($this->intervalList) - 1)
                    {
                        if ($tempInterval['state'] != 0) { array_push($filteredIncDecIntervals, $tempInterval); }
                    }
                }
                else
                {
                    // interval end
                    array_push($filteredIncDecIntervals, $tempInterval);
                    // start new interval
                    $tempInterval = $this->intervalList[$i];
                    // handle last interval
                    if ($i == sizeof($this->intervalList) - 1)
                    {
                        if ($tempInterval['state'] != 0) { array_push($filteredIncDecIntervals, $tempInterval); }
                    }
                }
            }
        }
        $this->intervalList = $filteredIncDecIntervals;
    }

    private function generateActionList()
    {
        // 0 -> sell
        $this->actionList = array_fill(0, sizeof($this->marketPrediction), 0);
        $selectorIdx = 0;
        
        foreach($this->intervalList as $interval)
        {
            if ($interval['state'] == 1)
            {
                $startID;
                ($interval['useSubRange'] > 0) ? $startID = $interval['subRangeStartIndex'] : $startID = $interval['startIndex'];

                for ($i = $startID; $i < $interval['endIndex']; $i++)
                {
                    $this->actionList[$i] = 1;
                }
            }

            if ($interval['state'] == 2)
            {   
                for ($i = $interval['startIndex']; $i < $interval['endIndex']; $i++)
                {
                    $this->actionList[$i] = 1;
                }
                
            }

            if ($interval['state'] == 0)
            {
                $this->actionList[$interval['endIndex']] = 1;
                if ($interval['useSubRange'] > 0)
                {
                    $this->actionList[$interval['subRangeEndIndex']] = 1;
                }
            }
        }
    }
}

//===================================================================================
// PROBLEM 2 ADDITIONAL METHODS & CLASSES
//===================================================================================

function longestCommonSubsequence(string $X, string $Y) : int
{
    $m = strLen($X);
    $n = strLen($Y);
    
    $L = array_fill(0, $m + 1, array_fill(0, $n + 1, 0));
    // use dynamic programming
	// Following steps build $L[$m+1][$n+1] in bottom up fashion.
    // Note that $L[$i][$j] contains length of LCS of $X[0..$i-1] and $Y[0..$j-1]
    for ($i = 0; $i <= $m; $i++) 
    {
        for ($j = 0; $j <= $n; $j++) 
        {
            if ($i == 0 || $j == 0) 
            {
                $L[$i][$j] = 0;
            } 
            elseif ($X[$i - 1] == $Y[$j - 1]) 
            {
                $L[$i][$j] = $L[$i - 1][$j - 1] + 1;
            } 
            else 
            {
                $L[$i][$j] = max($L[$i - 1][$j], $L[$i][$j - 1]);
            }
        }
    }
	// $L[$m][$n] contains length of LCS for $X[0..$n-1] and $Y[0..$m-1]
	return $L[$m][$n];
}

//===================================================================================
// PROBLEM 3 ADDITIONAL METHODS & CLASSES
//===================================================================================

class RunningMedian
{
    private $minHeap;
    private $maxHeap;
	private $currentMedian;
	private $stepID;
	private $maxSize;
    
    function __construct()
    {
        $this->minHeap = new SplMinHeap();
        $this->maxHeap = new SplMaxHeap();
        $this->currentMedian = 0;
        $this->stepID = 0;
        $this->maxSize = 0;
    }
    
	function __destruct() {}
    
	public function step(int $in, bool $calculateAverage) : int
    {
        if ($this->stepID == 0)
        {
            $this->maxHeap->insert($in);
            $this->currentMedian = $in;
        }
        else
        {
            // if $in is <= than current median, push it to the $_maxHeap; if not -> $_minHeap
            ($in <= $this->currentMedian) ? $this->maxHeap->insert($in) : $this->minHeap->insert($in);

            // check if heaps are balanced, rebalance if needed
            if (abs($this->maxHeap->count() - $this->minHeap->count()) > 1)
            {
                $this->rebalanceHeaps();
            }
            // get current median
            $this->currentMedian = $this->getMedian($calculateAverage);
        }

        $this->stepID++;
        return $this->currentMedian;
    }
    
    private function getMedian(bool $calculateAverage) : int
    {
        if ($this->minHeap->count() == $this->maxHeap->count())
        {
            if ($calculateAverage)
            {
                return ($this->minHeap->top() + $this->maxHeap->top()) * 0.5;
            }
            else
            {
                return min($this->minHeap->top(), $this->maxHeap->top());
            }
        }
        if ($this->minHeap->count() < $this->maxHeap->count())
        {
            return $this->maxHeap->top();
        }
        if ($this->minHeap->count() > $this->maxHeap->count())
        {
            return $this->minHeap->top();
        }
        return 0;
    }
    
	private function rebalanceHeaps()
    {
        ($this->minHeap->count() > $this->maxHeap->count())   ? 
            $this->maxHeap->insert($this->minHeap->extract()) :
            $this->minHeap->insert($this->maxHeap->extract()) ;
    }
};

?>
